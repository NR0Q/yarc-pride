#!/bin/bash

###########################################
## BASh script that generates the YARC Pride logo
##
## Matthew Chambers NR0Q 2023
##
###########################################

## init some ANSI Color codes
RED="\e[0;31m"
ORG="\e[0;33m"
YEL="\e[1;33m"
GRN="\e[0;32m"
BLU="\e[0;34m"
PRP="\e[0;35m"
WHT="\e[1;37m"
ENDCLR="\e[0m"

## clear the screen
clear


## first 2 lines solid RED
for j in {1..2}
do
    for i in {1..40}
    do
        echo -en "${RED}Y${ENDCLR}"
    done
    for i in {1..40}
    do 
        echo -en "${RED}A${ENDCLR}"
    done
    echo -en "\n"
done

## first line of Y and A RED

## Y
for i in {1..9}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..9}
do
    echo -en "${RED}Y${ENDCLR}"
done

## A
for i in {1..17}
do
    echo -en "${RED}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..17}
do
    echo -en "${RED}A${ENDCLR}"
done
echo -en "\n"

## second line of Y and A RED

## Y
for i in {1..10}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${RED}Y${ENDCLR}"
done

## A
for i in {1..16}
do
    echo -en "${RED}A${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..16}
do
    echo -en "${RED}A${ENDCLR}"
done
echo -en "\n"

## third line of Y and A RED

## Y
for i in {1..11}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..11}
do
    echo -en "${RED}Y${ENDCLR}"
done

## A
for i in {1..15}
do
    echo -en "${RED}A${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..15}
do
    echo -en "${RED}A${ENDCLR}"
done
echo -en "\n"

## fourth line of Y and A RED

## Y
for i in {1..12}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..4}
do
    echo -en "${RED}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${RED}Y${ENDCLR}"
done

## A
for i in {1..14}
do
    echo -en "${RED}A${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..14}
do
    echo -en "${RED}A${ENDCLR}"
done
echo -en "\n"

## fith line of Y and A ORG

## Y
for i in {1..13}
do
    echo -en "${ORG}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..2}
do
    echo -en "${ORG}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..13}
do
    echo -en "${ORG}Y${ENDCLR}"
done

## A
for i in {1..13}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..2}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..13}
do
    echo -en "${ORG}A${ENDCLR}"
done
echo -en "\n"

## sixth line of Y and A ORG

## Y
for i in {1..14}
do
    echo -en "${ORG}Y${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..14}
do
    echo -en "${ORG}Y${ENDCLR}"
done

# A
for i in {1..12}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..4}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${ORG}A${ENDCLR}"
done
echo -en "\n"

## seventh line of Y and A ORG
for i in {1..15}
do
    echo -en "${ORG}Y${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..15}
do
    echo -en "${ORG}Y${ENDCLR}"
done

## A
for i in {1..11}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..11}
do
    echo -en "${ORG}A${ENDCLR}"
done
echo -en "\n"

## eigth line of Y and A ORG

## Y
for i in {1..16}
do
    echo -en "${ORG}Y${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..16}
do
    echo -en "${ORG}Y${ENDCLR}"
done

## A
for i in {1..10}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..20}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${ORG}A${ENDCLR}"
done

echo -en "\n"

## nineth line of Y and A ORG

## Y
for i in {1..17}
do
    echo -en "${ORG}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..17}
do
    echo -en "${ORG}Y${ENDCLR}"
done

## A
for i in {1..9}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..22}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..9}
do
    echo -en "${ORG}A${ENDCLR}"
done

echo -en "\n"

## 10th line of Y and A ORG

## Y
for i in {1..17}
do
    echo -en "${ORG}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..17}
do
    echo -en "${ORG}Y${ENDCLR}"
done

## A
for i in {1..8}
do
    echo -en "${ORG}A${ENDCLR}"
done
for i in {1..24}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${ORG}A${ENDCLR}"
done
echo -en "\n"

## 11th line of Y and A YEL

## Y
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done

## A
for i in {1..7}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..14}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..7}
do
    echo -en "${YEL}A${ENDCLR}"
done

echo -en "\n"

## 12th line of Y and A YEL

## Y
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done

## A
for i in {1..6}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..16}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${YEL}A${ENDCLR}"
done

echo -en "\n"

## 13th line of Y and A YEL

## A
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done

## A
for i in {1..5}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..18}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..5}
do
    echo -en "${YEL}A${ENDCLR}"
done

echo -en "\n"

## 14th line of Y and A YEL

## Y
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}Y${ENDCLR}"
done
for i in {1..17}
do
    echo -en "${YEL}Y${ENDCLR}"
done

## A
for i in {1..4}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..20}
do
    echo -en "${YEL}A${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}A${ENDCLR}"
done
for i in {1..4}
do
    echo -en "${YEL}A${ENDCLR}"
done

echo -en "\n"

## 2 lines of solid YEL
for i in {1..2}
do
    for i in {1..40}
    do
        echo -en "${YEL}Y${ENDCLR}"
    done
    for i in {1..40}
    do
        echo -en "${YEL}A${ENDCLR}"
    done
    echo -en "\n"
done

## 2 lines of solid GRN
for i in {1..2}
do
    for i in {1..40}
    do
        echo -en "${GRN}R${ENDCLR}"
    done
    for i in {1..40}
    do
        echo -en "${GRN}C${ENDCLR}"
    done
    echo -en "\n"
done

## first line of R and C GRN

## R
for i in {1..8}
do
    echo -en "${GRN}R${ENDCLR}"
done
for i in {1..18}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..14}
do
    echo -en "${GRN}R${ENDCLR}"
done

## C
for i in {1..13}
do
    echo -en "${GRN}C${ENDCLR}"
done
for i in {1..14}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..13}
do
    echo -en "${GRN}C${ENDCLR}"
done

echo -en "\n"

## second line of R and C GRN

## R
for i in {1..8}
do
    echo -en "${GRN}R${ENDCLR}"
done
for i in {1..20}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${GRN}R${ENDCLR}"
done

## C
for i in {1..11}
do
    echo -en "${GRN}C${ENDCLR}"
done
for i in {1..18}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..11}
do
    echo -en "${GRN}C${ENDCLR}"
done

echo -en "\n"

## third line of R and C GRN

## R
for i in {1..8}
do
    echo -en "${GRN}R${ENDCLR}"
done
for i in {1..21}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..11}
do
    echo -en "${GRN}R${ENDCLR}"
done

## C
for i in {1..9}
do
    echo -en "${GRN}C${ENDCLR}"
done
for i in {1..22}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..9}
do
    echo -en "${GRN}C${ENDCLR}"
done

echo -en "\n"

## fourth line of R and C GRN

## R
for i in {1..8}
do
    echo -en "${GRN}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${GRN}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${GRN}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${GRN}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${GRN}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${GRN}C${ENDCLR}"
done
echo -en "\n"

## fifth line of R and C GRN

## R
for i in {1..8}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${BLU}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done
echo -en "\n"

## sixth line of R and C BLU

## R
for i in {1..8}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${BLU}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..26}
do
    echo -en "${BLU}C${ENDCLR}"
done

echo -en "\n"

## seventh line of R and C BLU

## R
for i in {1..8}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..21}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..11}
do
    echo -en "${BLU}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..26}
do
    echo -en "${BLU}C${ENDCLR}"
done

echo -en "\n"


## eigth line of R and C BLU

## R
for i in {1..8}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..20}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${BLU}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..26}
do
    echo -en "${BLU}C${ENDCLR}"
done

echo -en "\n"

## nineth line of R and C BLU

## R
for i in {1..8}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..21}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..11}
do
    echo -en "${BLU}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..26}
do
    echo -en "${BLU}C${ENDCLR}"
done

echo -en "\n"

## 10th line of R and C BLU

## R
for i in {1..8}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${BLU}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${BLU}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${BLU}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${BLU}C${ENDCLR}"
done

echo -en "\n"

## 11th line of R and C PRP

## R
for i in {1..8}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done

## C
for i in {1..8}
do
    echo -en "${PRP}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..12}
do
    echo -en "${PRP}C${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..8}
do
    echo -en "${PRP}C${ENDCLR}"
done

echo -en "\n"

## 12th line of R and C PRP

## R
for i in {1..8}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done

## C
for i in {1..9}
do
    echo -en "${PRP}C${ENDCLR}"
done
for i in {1..22}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..9}
do
    echo -en "${PRP}C${ENDCLR}"
done
echo -en "\n"

## 13th line of R and C PRP

## R
for i in {1..8}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done

## C
for i in {1..11}
do
    echo -en "${PRP}C${ENDCLR}"
done
for i in {1..18}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..11}
do
    echo -en "${PRP}C${ENDCLR}"
done

echo -en "\n"

## 14th line of R and C PRP

## R
for i in {1..8}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done
for i in {1..6}
do
    echo -en "${WHT}R${ENDCLR}"
done
for i in {1..10}
do
    echo -en "${PRP}R${ENDCLR}"
done

## C
for i in {1..13}
do
    echo -en "${PRP}C${ENDCLR}"
done
for i in {1..14}
do
    echo -en "${WHT}C${ENDCLR}"
done
for i in {1..13}
do
    echo -en "${PRP}C${ENDCLR}"
done
echo -en "\n"


# 2 lines of solid PRP
for i in {1..2}
do
    for i in {1..40}
    do
        echo -en "${PRP}R${ENDCLR}"
    done
    for i in {1..40}
    do
        echo -en "${PRP}C${ENDCLR}"
    done
    echo -en "\n"
done
